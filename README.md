allows you to select hasOne when editing or creating

## Install
```bash
composer require enmaboya/relation-select
```

***
## Using
Suppose we have a store and its director.
At the shop:
```php
     public function director ()
     {
         return $this-> hasOne (StoreDirector :: class);
     }
```

At the director:

```php
     public function store ()
     {
         return $this-> belongsTo (Store :: class);
     }
```

Next in store resource  add.

```php
public function fields(Request $request)
    {
        return [
            RelationSelect::make('director')->options([
    		'1' => 'Name',
    		'2' => 'Name',
    		'3' => 'Name',
	])->relateClass('App\StoreDirector')
                ->resolveUsing(function($director) { 
                    return $director->id; 
                })
                ->displayUsing(function ($director) {
                    return $director->name;
            }),
        ];
    }
```
