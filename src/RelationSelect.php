<?php

namespace Enmaboya\RelationSelect;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Select;

class RelationSelect extends Select
{
    public $relClass;
    public $component = 'relation-select';

    public function relateClass($relclass){
        $this->relClass = $relclass;
        return $this->withMeta([
            'relateClass' => $relclass
        ]);
    }

    protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
	{
        $class = get_class($model);
		$class::saved(function ($model) use ($requestAttribute, $request) {
            $field = $this->relClass::find($request[$requestAttribute]);
            $field->restaurant_id = $model->id;
            $field->save();
        });
    }
}
